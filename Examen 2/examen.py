import math
MaxSize = float('inf')


def tempo(camino):
	finCamino[:N + 1] = camino[:]
	finCamino[N] = camino[0]
#=======================================================================================================================================================
def Pmin(dis, i):
	min = MaxSize
	for k in range(N):
		if dis[i][k] < min and i != k:
			min = dis[i][k]
	return min
#========================================================================================================================================================
def Smin(dis, i):
	primer, segundo = MaxSize, MaxSize
	for j in range(N):
		if i == j:
			continue
		if dis[i][j] <= primer:
			segundo = primer
			primer = dis[i][j]

		elif(dis[i][j] <= segundo and
			dis[i][j] != primer):
			segundo = dis[i][j]
	return segundo
#=======================================================================================================================================================
def recursi(dis, limiteAct, pesoAct,
			nivel, camino, visited):
	global resFin

	if nivel == N:
		if dis[camino[nivel - 1]][camino[0]] != 0:
			curr_res = pesoAct + dis[camino[nivel - 1]]\
										[camino[0]]
			if curr_res < resFin:
				tempo(camino)
				resFin = curr_res
		return

	for i in range(N):
		if (dis[camino[nivel-1]][i] != 0 and
							visited[i] == False):
			temp = limiteAct
			pesoAct += dis[camino[nivel - 1]][i]
			if nivel == 1:
				limiteAct -= ((Pmin(dis, camino[nivel - 1]) +
								Pmin(dis, i)) / 2)
			else:
				limiteAct -= ((Smin(dis, camino[nivel - 1]) +
								Pmin(dis, i)) / 2)
			if limiteAct + pesoAct < resFin:
				camino[nivel] = i
				visited[i] = True

				recursi(dis, limiteAct, pesoAct,
					nivel + 1, camino, visited)

			pesoAct -= dis[camino[nivel - 1]][i]
			limiteAct = temp

			visited = [False] * len(visited)
			for j in range(nivel):
				if camino[j] != -1:
					visited[camino[j]] = True
#=======================================================================================================================================================
def TSP(dis):

	limiteAct = 0
	caminoAct = [-1] * (N + 1)
	visited = [False] * N

	for i in range(N):
		limiteAct += (Pmin(dis, i) +
					Smin(dis, i))

	limiteAct = math.ceil(limiteAct / 2)

	visited[0] = True
	caminoAct[0] = 0

	recursi(dis, limiteAct, 0, 1, caminoAct, visited)
#=======================================================================================================================================================
dis = [[0, 20, 10, 100, 250],
	[20, 0, 180, 50, 50],
	[10, 180, 0, 200, 20],
    [100, 50, 200, 0, 80],
	[250, 50, 20, 80, 0]]
N = 5

finCamino = [None] * (N + 1)
visited = [False] * N
resFin = MaxSize

TSP(dis)
#------------------------------------------------------------------------------------------------------------------------------------------------------
print("Costo del viaje :", resFin)
print("\nCamino que se recorrio : ", end = ' ')
for i in range(N + 1):
	print(finCamino[i], end = ' ')